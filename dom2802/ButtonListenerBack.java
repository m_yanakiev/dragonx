package ButtonListeners;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Panels.StartPanel;
public class ButtonListenerBack implements ActionListener{
	StartPanel spanel;
	public ButtonListenerBack(StartPanel sp){
		spanel=sp;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		spanel.instpanel.setVisible(false);
	}
}
