package Panels;
import java.awt.Button;
import java.awt.Label;
import java.awt.Panel;
import ButtonListeners.ButtonListener2;
import ButtonListeners.ButtonListenerBack;
import ButtonListeners.ButtonListenerStart;
public class StartPanel extends Panel{
	public InstructionsPanel instpanel=new InstructionsPanel();
	public StartPanel(){
		//PANEL ZA INSTRUKCII
		this.instpanel.setVisible(false);
		this.instpanel.setBounds(0, 0, 1920, 1080);
		this.instpanel.setLayout(null);
		this.add(instpanel);
		//BUTON ZA IZLIZANE OT PANELA ZA INSTRUKCII
		ButtonListenerBack blb = new ButtonListenerBack(this);
		this.instpanel.back.addActionListener(blb);
		//BUTON ZA START NA IGRATA
		ButtonListenerStart gbls = new ButtonListenerStart(this);
		Button start =  new Button();
		start.setBounds(530, 350, 200, 50);
		start.setLabel("START!");
		start.addActionListener(gbls);
		this.add(start);
		//NADPIS V NACHALOTO NA IGRATA
		Label welcome = new Label();
		welcome.setBounds(560, 200, 200, 50);
		welcome.setText("WELCOME TO DRAGONX!");
		this.add(welcome);
		//BUTON ZA INSTRUKCII
		ButtonListener2 ins=new ButtonListener2(this);
		Button instruktions=new Button();
		instruktions.setBounds(530,450,200,50);
		instruktions.setLabel("Press here for instructions!");
		instruktions.addActionListener(ins);
		this.add(instruktions);
	}
}
