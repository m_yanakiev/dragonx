package Panels;
import java.awt.Button;
import java.awt.Panel;
import ButtonListeners.ButtonListener;
public class GamePanelToolBox extends Panel {
	public GamePanelToolBox() {
		ButtonListener gbl = new ButtonListener(this);
		Button button = new Button();
		button.setBounds(300, 350, 200, 50);
		button.setLabel("Press here to exit!");
		button.addActionListener(gbl);
		this.add(button);
	}
}
