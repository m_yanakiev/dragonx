package dom2802;

public class Karta2 {
	public Drakon d1,d2;
	public int rx,ry;
	int brh=0;
	public DragonHunter[] dharr=new DragonHunter[4];
	public void addHunter(DragonHunter x) {
		dharr[brh]=x;
		brh++;
	}
	public Karta2(int x,int y){
		setRx(x);
		setRy(y);
	}
	//GETTEEEEEEEEEEEEEEEEERS
	public int getRx() {
		return rx;
	}
	public int getRy() {
		return ry;
	}
	//SETTEEEEEEEEEEEEEEEEERS
	public void setRx(int x) {
		if (x>=0) rx = x;
		else System.out.println("Nevalidni razmeri na kartata!");
	}
	public void setRy(int y) {
		if (y>=0) ry = y;
		else System.out.println("Nevalidni razmeri na kartata!");
	}
	public void setDrakon1(Drakon x) {
		d1=x;
	}
	public void setDrakon2(Drakon x) {
		d2=x;
	}
	public void print() {
		for (int i=0;i<this.rx;i++){
			for (int j=0;j<this.ry;j++) {
				if (i==d1.getKordx() && j==d1.getKordy() || i==d2.getKordx() && j==d2.getKordy()) System.out.print("D ");
				else if (i==dharr[0].kordx && j==dharr[0].kordy && dharr[0]!=null || dharr[1]!=null && i==dharr[1].kordx && j==dharr[1].kordy || dharr[2]!=null && i==dharr[2].kordx && j==dharr[2].kordy || dharr[3]!=null && i==dharr[3].kordx && j==dharr[3].kordy) System.out.print("H ");
				else System.out.print("* ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
