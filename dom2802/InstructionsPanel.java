package Panels;
import java.awt.Button;
import java.awt.Panel;
import java.awt.TextArea;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class InstructionsPanel extends Panel{
	public TextArea textArea=new TextArea();
	public Button back = new Button();
	public InstructionsPanel() {
		textArea.setBounds(400, 200, 400, 150);
		File file=new File("instrukcii.txt");
		try {
			Scanner sc = new Scanner(file);
			String s="";
			while(sc.hasNextLine()) {
				s=s+sc.nextLine();
				s=s+"\n";
			}
			this.textArea.setText(s);
			sc.close();
		} catch (FileNotFoundException e1) {
			System.out.println("Nqma takuv fail!");
		}
		this.add(textArea);
		back.setBounds(450, 400, 200, 50);
		back.setLabel("Back");
		this.add(back);
	}
}
