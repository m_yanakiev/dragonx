package dom2802;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import Panels.PanelKarta2;
public class KeyboardSlusha2 implements KeyListener{
	PanelKarta2 panelk2;
	public KeyboardSlusha2(PanelKarta2 pk2){
		panelk2=pk2;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		char a=e.getKeyChar();
		if (a=='w') panelk2.karta2.d1.dvijisenagore();
		if (a=='s') panelk2.karta2.d1.dvijisenadolu();
		if (a=='a') panelk2.karta2.d1.dvijisenadqsno();
		if (a=='d') panelk2.karta2.d1.dvijisenalqvo();
		panelk2.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int c=e.getKeyCode();
		switch(c) {
		case 38:  panelk2.karta2.d2.dvijisenagore(); panelk2.repaint();break;
		case 40:  panelk2.karta2.d2.dvijisenadolu(); panelk2.repaint();break;
		case 37:  panelk2.karta2.d2.dvijisenadqsno(); panelk2.repaint();break;
		case 39:  panelk2.karta2.d2.dvijisenalqvo(); panelk2.repaint();break;
		default: c = ' ';
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
