package dom2802;
import java.awt.Frame;
import Panels.GamePanelToolBox;
import Panels.InstructionsPanel;
import Panels.PanelKarta;
import Panels.PanelKarta2;
import Panels.StartPanel;
public class Frame1 extends Frame{
	public Karta karta;
	public Karta2 karta2;
	InstructionsPanel inspanel;
	public PanelKarta panelk;
	public GamePanelToolBox panelt;
	StartPanel spanel;
	public PanelKarta2 panelk2;
	Frame1(Karta k, Karta2 k2) {
		karta = k;
		karta2=k2;
		Windowslusha gwl = new Windowslusha(this);
		addWindowListener(gwl);
		this.setLayout(null);
		
		panelt=new GamePanelToolBox(this);
		panelt.setBounds(521, 20, 500, 500);
		panelt.setLayout(null);
		panelt.setVisible(false);
		this.add(panelt);

		panelk = new PanelKarta(k);
		panelk.setBounds(20, 20, 500, 500);
		panelk.setLayout(null);
		this.add(panelk);
		panelk.setVisible(false);
		
		spanel=new StartPanel(this);
		spanel.setBounds(0,0,1920,1080);
		spanel.setLayout(null);
		spanel.setVisible(true);
		this.add(spanel);
		
		panelk2 = new PanelKarta2(k2);
		panelk2.setBounds(20, 20, 500, 500);
		panelk2.setLayout(null);
		panelk2.setVisible(false);
		this.add(panelk2);
	}
}
