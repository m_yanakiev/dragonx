package ButtonListeners;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import Panels.StartPanel;
import dom2802.DragonHunter;
import dom2802.Frame1;
public class ButtonListenerStart implements ActionListener{
	StartPanel spanel;
	DragonHunter dh1=new DragonHunter(0,3);DragonHunter dh2=new DragonHunter(2,0);
	DragonHunter dh3=new DragonHunter(4,7);DragonHunter dh4=new DragonHunter(1,5);
	int i=0;
	int bx=0,by=0;
	Random rand = new Random();
	int upperbound;
	Frame1 frame;
	Timer xptimer;
	public ButtonListenerStart(StartPanel sp, Frame1 f) {
		xptimer = new Timer(1500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bx = rand.nextInt(upperbound); 
	    		by = rand.nextInt(upperbound); 
	    		frame.karta.kxp=bx; frame.karta.kyp=by;
	    		frame.karta.print();
	    		System.out.println();
	    		frame.panelk.repaint();
	    		i++;
	    		frame.panelt.repaint();
	    		if (i==9) xptimer.setRepeats(false);
	    		if (i==10) {
	    			frame.karta.kxp=-1; frame.karta.kyp=-1;
	    			frame.panelt.ad1.setText("Drakon1 Attack Points - "+ frame.karta.d1.attp);
	    			frame.panelt.ad2.setText("Drakon2 Attack Points - "+ frame.karta.d2.attp);
	    			frame.panelt.hd1.setText("Drakon1 Health Points - "+ frame.karta.d1.hp);
	    			frame.panelt.hd2.setText("Drakon2 Health Points - "+ frame.karta.d2.hp);
	    			frame.add(frame.panelt);
	    			frame.panelk.setVisible(false);
	    			frame.panelk2.karta2.addHunter(dh1);frame.panelk2.karta2.addHunter(dh2);
	    			frame.panelk2.karta2.addHunter(dh3);frame.panelk2.karta2.addHunter(dh4);
	    			frame.panelk2.karta2.d1.kordx=8; frame.panelk2.karta2.d1.kordy=8;
	    			frame.panelk2.karta2.d2.kordx=8; frame.panelk2.karta2.d2.kordy=0;
	    			frame.panelk2.setVisible(true);
	    		}
			}
		});
		spanel=sp;
		frame=f;
		upperbound=frame.karta.getRx();	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		spanel.setVisible(false);
		frame.panelk.setVisible(true);
		frame.panelt.setVisible(true);
		xptimer.start();
	}
}
