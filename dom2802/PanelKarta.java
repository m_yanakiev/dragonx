package Panels;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import dom2802.Karta;
import dom2802.KeyboardSlusha;
public class PanelKarta extends Panel{
	public Karta karta;
	public PanelKarta(Karta k) {
		karta = k;
		KeyboardSlusha gkl=new KeyboardSlusha(this);
		addKeyListener(gkl);
	}
	public void paint(Graphics g) {
		BufferedImage img=null;
		try {
			img=ImageIO.read(new File("mbukva.jpg"));
		}catch (IOException e) {
			e.printStackTrace();
		}
		//otpechatvane na kartata
		for (int i=0; i < karta.getRy(); i++){
			for (int j=0; j < karta.getRx(); j++) {
				if (j==karta.d1.getKordx() && i==karta.d1.getKordy() || j==karta.d2.getKordx() && i==karta.d2.getKordy()) {
					g.drawImage(img, 40+i*50, 40+j*50, null);
				}
				else {g.setColor(java.awt.Color.black); g.drawRect(40+i*50, 40+j*50, 50, 50);}
			}
		}	
	}
}
