package dom2802;
public class Karta {
	private int rx,ry;
	public int kxp=2,kyp=2;
	public Drakon d1,d2;
	Karta(int x,int y){
		setRx(x);
		setRy(y);
	}
	//GETTEEEEEEEEEEEEEEEEERS
	public int getRx() {
		return rx;
	}
	public int getRy() {
		return ry;
	}
	//SETTEEEEEEEEEEEEEEEEERS
	public void setRx(int x) {
		if (x>=0) rx = x;
		else System.out.println("Nevalidni razmeri na kartata!");
	}
	public void setRy(int y) {
		if (y>=0) ry = y;
		else System.out.println("Nevalidni razmeri na kartata!");
	}
	public void setDrakon1(Drakon x) {
		d1=x;
	}
	public void setDrakon2(Drakon x) {
		d2=x;
	}
	//PRINTIRANE NA KARTATA
	public void print() {
		for (int i=0;i<rx;i++){
			for (int j=0;j<ry;j++) {
				if (i==d1.getKordx() && j==d1.getKordy() || i==d2.getKordx() && j==d2.getKordy()) System.out.print("D ");
				else if (i==kxp && j==kyp) System.out.print("P ");
				else System.out.print("* ");
			}
			System.out.println();
		}
		System.out.println();
	}
}

