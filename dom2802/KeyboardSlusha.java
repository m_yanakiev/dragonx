package dom2802;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import Panels.PanelKarta;
public class KeyboardSlusha implements KeyListener{
	PanelKarta panelk;
	public KeyboardSlusha(PanelKarta pk){
		panelk=pk;
	}
	@Override
	public void keyTyped(KeyEvent e) {
		char a=e.getKeyChar();
		if (a=='w') panelk.karta.d1.dvijisenagore();
		if (a=='s') panelk.karta.d1.dvijisenadolu();
		if (a=='a') panelk.karta.d1.dvijisenadqsno();
		if (a=='d') panelk.karta.d1.dvijisenalqvo();
		panelk.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int c=e.getKeyCode();
		switch(c) {
		case 38:  panelk.karta.d2.dvijisenagore(); panelk.repaint();break;
		case 40:  panelk.karta.d2.dvijisenadolu(); panelk.repaint();break;
		case 37:  panelk.karta.d2.dvijisenadqsno(); panelk.repaint();break;
		case 39:  panelk.karta.d2.dvijisenalqvo(); panelk.repaint();break;
		default: c = ' ';
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
