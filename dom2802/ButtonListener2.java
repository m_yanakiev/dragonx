package ButtonListeners;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Panels.StartPanel;
public class ButtonListener2 implements ActionListener {
	StartPanel stpanel;
	public ButtonListener2(StartPanel sp) {
		stpanel=sp;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		stpanel.instpanel.setVisible(true);
	}
}

